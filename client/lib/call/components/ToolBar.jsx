import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "@atlaskit/button";
import VidAudioOnIcon from "@atlaskit/icon/glyph/vid-audio-on";
import VidAudioMutedIcon from "@atlaskit/icon/glyph/vid-audio-muted";
import VidCameraOnIcon from "@atlaskit/icon/glyph/vid-camera-on";
import VidCameraOffIcon from "@atlaskit/icon/glyph/vid-camera-off";
import VidHangUpIcon from "@atlaskit/icon/glyph/vid-hang-up";
import VidFullScreenOnIcon from "@atlaskit/icon/glyph/vid-full-screen-on";
import VidFullScreenOffIcon from "@atlaskit/icon/glyph/vid-full-screen-off";
import VidShareScreenIcon from "@atlaskit/icon/glyph/vid-share-screen";
import PropTypes from "prop-types";
import ToolButton from "./ToolButton";

class ToolBar extends Component {
  element = document.getElementById("app");

  render() {
    const className = this.props.visible ? "toolbar" : "toolbar toolbar-hide";
    return (
      <div className={className}>
        <ToolButton
          onClick={this.props.publishAudio}
          toggledIcon={
            <VidAudioOnIcon
              primaryColor="rgba(255, 255, 255, 1)"
              size="large"
            />
          }
          untoggledIcon={
            <VidAudioMutedIcon
              primaryColor="rgba(48, 197, 246, 1)"
              size="large"
            />
          }
        />
        <ToolButton
          onClick={this.props.publishVideo}
          toggledIcon={
            <VidCameraOnIcon
              primaryColor="rgba(255, 255, 255, 1);"
              size="large"
            />
          }
          untoggledIcon={
            <VidCameraOffIcon
              primaryColor="rgba(48, 197, 246, 1)"
              size="large"
            />
          }
        />
        <Link to="/main">
          <Button
            iconBefore={
              <VidHangUpIcon primaryColor="rgba(213, 4, 29, 1)" size="large" />
            }
            onClick={this.props.leaveSession}
            appearance="link"
          />
        </Link>
        <ToolButton
          rootElement={this.element}
          toggledIcon={
            <VidFullScreenOnIcon
              primaryColor="rgba(255, 255, 255, 1)"
              size="large"
            />
          }
          untoggledIcon={
            <VidFullScreenOffIcon
              primaryColor="rgba(48, 197, 246, 1)"
              size="large"
            />
          }
          onClick={isToggled => {
            // eslint-disable-next-line no-unused-expressions
            isToggled
              ? this.props.openFullScreen(this.element)
              : this.props.closeFullScreen();
          }}
        />
        <ToolButton
          toggledIcon={
            <VidShareScreenIcon
              primaryColor="rgba(255, 255, 255, 1)"
              size="large"
            />
          }
          untoggledIcon={
            <VidShareScreenIcon
              primaryColor="rgba(48, 197, 246, 1)"
              size="large"
            />
          }
          onClick={isToggled => {
            // eslint-disable-next-line no-unused-expressions
            isToggled ? this.props.shareScreen() : this.props.unshareScreen();
          }}
        />
      </div>
    );
  }
}

export default ToolBar;

ToolBar.propTypes = {
  visible: PropTypes.bool,
  leaveSession: PropTypes.func.isRequired,
  shareScreen: PropTypes.func.isRequired,
  unshareScreen: PropTypes.func.isRequired,
  closeFullScreen: PropTypes.func.isRequired,
  openFullScreen: PropTypes.func.isRequired,
  publishAudio: PropTypes.func.isRequired,
  publishVideo: PropTypes.func.isRequired,
};
