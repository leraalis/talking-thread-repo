import React, { Component } from "react";
import Avatar, { AvatarItem } from "@atlaskit/avatar";

export default class CurrentUsers extends Component {
  users = [
    {
      name: "Scarlett Johansson",
      src: "https://robohash.org/U5O.png?set=set4",
    },
    {
      name: "Jason Statham",
      src: "https://robohash.org/G6P.png?set=set4",
    },
  ];

  render() {
    return (
      <div className="usersBlock">
        <div className="users">
          {this.users.map(user => (
            <AvatarItem
              avatar={
                <Avatar
                  name={user.name}
                  src={user.src}
                  size="large"
                  presence="online"
                />
              }
              key={user.name}
              primaryText={user.name}
            />
          ))}
          <p>{this.users.name}</p>
        </div>
      </div>
    );
  }
}
