import {
  SET_MAIN_PARTICIPANT,
  ADD_PARTICIPANT,
  REMOVE_PARTICIPANT,
  UPDATE_PARTICIPANT,
  SET_PARTICIPANTS,
  SCREEN_SHARING_ENABLED,
} from "../actionTypes";

const initialState = {
  mainParticipant: "",
  participants: [],
  isScreenSharingEnabled: false,
};

function splitParticipants(acc, participant) {
  if (participant.isRemote) {
    acc.subscribers.push(participant);
  } else {
    acc.publisher = participant;
  }

  return acc;
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SCREEN_SHARING_ENABLED: {
      return { ...state, isScreenSharingEnabled: action.payload };
    }
    case SET_MAIN_PARTICIPANT: {
      return { ...state, mainParticipant: action.login };
    }
    case ADD_PARTICIPANT: {
      const { currentUser, user } = action;
      const { publisher, subscribers } = state.participants
        .concat({
          name: user.name,
          login: user.login,
          isRemote: user.login !== currentUser.login,
          mediaStream: null,
          isVideoEnabled: user.isVideoEnabled || true,
          isAudioEnabled: user.isAudioEnabled || true,
        })
        .reduce(splitParticipants, { publisher: null, subscribers: [] });

      return {
        ...state,
        participants: [publisher, ...subscribers],
      };
    }

    case REMOVE_PARTICIPANT: {
      const { publisher, subscribers } = state.participants
        .filter(participant => participant.login !== action.user.login)
        .reduce(splitParticipants, { publisher: null, subscribers: [] });

      return {
        ...state,
        participants: [publisher, ...subscribers],
      };
    }
    case SET_PARTICIPANTS: {
      const { publisher, subscribers } = action.users
        .map(user => ({
          name: user.name,
          login: user.login,
          isRemote: user.login !== action.currentUser.login,
          mediaStream: null,
          isVideoEnabled: user.isVideoEnabled || true,
          isAudioEnabled: user.isAudioEnabled || true,
        }))
        .reduce(splitParticipants, { publisher: null, subscribers: [] });

      return {
        ...state,
        participants: [publisher, ...subscribers],
      };
    }
    case UPDATE_PARTICIPANT: {
      const { login, participant: updatedParticipant } = action;

      return {
        ...state,
        participants: state.participants.reduce((acc, participant) => {
          if (participant.login === login) {
            acc.push({
              ...participant,
              ...updatedParticipant,
            });
          } else {
            acc.push(participant);
          }

          return acc;
        }, []),
      };
    }

    default:
      return state;
  }
}
