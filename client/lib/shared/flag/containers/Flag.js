import { connect } from "react-redux";
import { dismissFlag } from "../actions";
import FlagComponent from "../components/Flag";

const mapStateToProps = state => ({
  flags: state.flags.flags,
});

const mapDispatchToProps = dispatch => ({
  onDismiss: id => dispatch(dismissFlag(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FlagComponent);
