import React from "react";
import "./style.scss";
import Main from "./containers/Main";
import Canvas from "./components/Canvas";
import Init from "../init";

function App(props) {
  return (
    <Init>
      <div>
        <Canvas />
        {/* eslint-disable-next-line react/prop-types */}
        <Main history={props.history} />
      </div>
    </Init>
  );
}

export default App;
