export const LOGIN_REQUEST = "login/LOGIN_REQUEST";
export const LOGIN_SUCCESS = "login/LOGIN_SUCCESS";
export const LOGIN_ERROR = "login/LOGIN_ERROR";
export const LOGOUT_REQUEST = "login/LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "login/LOGOUT_SUCCESS";
export const LOGOUT_ERROR = "login/LOGOUT_ERROR";
export const SIGN_IN_FORM_OPENED = "login/SIGN_IN_FORM_OPENED";
export const SIGN_UP_FORM_OPENED = "login/SIGN_UP_FORM_OPENED";
export const SIGN_IN_MODAL_OPENED = "login/SIGN_IN_MODAL_OPENED";
export const SIGN_IN_MODAL_CLOSED = "login/SIGN_IN_MODAL_CLOSED";
