import { push } from "connected-react-router";
import localStorage from "../sessions/LocalStorageService";
import { APIService } from "../sessions";
import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  SIGN_IN_FORM_OPENED,
  SIGN_UP_FORM_OPENED,
  SIGN_IN_MODAL_OPENED,
  SIGN_IN_MODAL_CLOSED,
} from "./actionTypes";

import { showFlag } from "../shared/flag";

export const loginSuccess = (login, password) => ({
  type: LOGIN_SUCCESS,
  payload: {
    login,
    password,
  },
});

export const loginError = () => ({
  type: LOGIN_ERROR,
});

function isLoginPage() {
  return window.location.pathname === "/login";
}

export const loginRequest = (login, password) => async dispatch => {
  const body = { login, password };
  try {
    const result = await APIService.post(
      "api/auth/login",
      { body },
      "Login WRONG",
    );
    localStorage.set("accessToken", result.accessToken);
    localStorage.set("id", result.id);
    dispatch(loginSuccess(login, password));
    if (isLoginPage()) dispatch(push("/main"));
    dispatch(showFlag("success", `You've successfully logged in as ${login}`));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.warn("Error. Can't logIn", e);
    dispatch(loginError());
    dispatch(
      showFlag(
        "error",
        "Oops, something went wrong",
        "You entered an incorrect login or password",
      ),
    );
  }
};

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

export const logoutError = () => ({
  type: LOGOUT_ERROR,
});

export const logoutRequest = () => async dispatch => {
  try {
    await APIService.post("api/auth/logout", {}, "Logout WRONG");
    localStorage.delete("accessToken");
    localStorage.delete("id");
    dispatch(logoutSuccess());
    dispatch(showFlag("success", "You've successfully logged out"));
    dispatch(push("/"));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.warn("Error. Can't logOut", e);
    dispatch(showFlag("error", "Error logging out"));
    dispatch(logoutError());
  }
};

export const openSignInForm = () => ({
  type: SIGN_IN_FORM_OPENED,
});

export const openSignUpForm = () => ({
  type: SIGN_UP_FORM_OPENED,
});

export const openSignInModal = () => ({
  type: SIGN_IN_MODAL_OPENED,
});

export const closeSignInModal = () => ({
  type: SIGN_IN_MODAL_CLOSED,
});

export const createAccountRequest = data => async dispatch => {
  const body = {
    name: data.userName,
    login: data.userLogin,
    password: data.userPassword,
  };
  try {
    const result = await APIService.post("api/users", { body }, "signUp WRONG");
    localStorage.set("accessToken", result.accessToken);
    localStorage.set("id", result.id);
    dispatch(openSignInForm());
    dispatch(loginSuccess(data.userLogin));
    dispatch(push("/main"));
    dispatch(
      showFlag("success", `You've successfully logged in as ${data.userLogin}`),
    );
  } catch (e) {
    // eslint-disable-next-line no-console
    console.warn("Error. Can't signUp", e);
    dispatch(loginError());
    dispatch(
      showFlag("error", "Oops, something went wrong", "Can`t create account"),
    );
  }
};
