import { connect } from "react-redux";
import {
  loginRequest,
  openSignInForm,
  openSignUpForm,
  openSignInModal,
  closeSignInModal,
} from "../actions";
import LoginDialog from "../components/LoginDialog";

const mapStateToProps = state => ({
  formState: state.login.formReducer,
  user: {
    login: state.login.userReducer.login,
    password: state.login.userReducer.password,
    publisher: state.login.userReducer.publisher,
  },
  sessionName: state.main.sessionName,
});

const mapDispatchToProps = dispatch => ({
  onLogin: (login, password) => dispatch(loginRequest(login, password)),
  onOpenSignInForm: () => dispatch(openSignInForm()),
  onOpenSignUpForm: () => dispatch(openSignUpForm()),
  onOpenSignInModal: () => dispatch(openSignInModal()),
  onCloseSignInModal: () => dispatch(closeSignInModal()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginDialog);
