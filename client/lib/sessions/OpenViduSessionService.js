import APIService from "./APIService";
import { setMainParticipant, updateParticipant } from "../call/actions";
import store from "../index";

class OpenViduSessionService {
  constructor() {
    this.session = null;
    this.sessionName = null; // Name of the video session the user will connect to
    this.token = null; // Token retrieved from OpenVidu Server
    this.publisher = null;
    this.subscribers = [];
    this.isVideoStreaming = true;
    this.isAudioStreaming = true;
    this.OV = new window.OpenVidu();
  }

  async joinSession(user, sessionName) {
    return new Promise(async (resolve, reject) => {
      this.sessionName = sessionName;
      this.session = this.OV.initSession();
      // On every new Stream received...

      console.log("session initialized");
      this.session.on("streamCreated", event => {
        console.log("stream created", event);
        // Subscribe to the Stream to receive it
        // HTML video will be appended to element with 'video-container' id

        const { data: rawSubscriberData } = event.stream.connection;
        const rawSubscriberClientData = rawSubscriberData.split("%/%")[0];
        const { clientData: login } = JSON.parse(rawSubscriberClientData);

        const subscriber = this.session.subscribe(
          event.stream,
          `smallVideo-${login}`,
        );
        // When the HTML video has been appended to DOM...

        subscriber.on("streamPlaying", () => {
          store.dispatch(
            updateParticipant(login, {
              mediaStream: subscriber.stream.mediaStream,
            }),
          );
          store.dispatch(setMainParticipant(login));
        });
      });

      // Connect to the session passing the retrieved token and some more data from
      // the client (in this case a JSON with the nickname chosen by the user) ---

      this.session
        .connect(this.token, { clientData: user.login })
        .then(resolve)
        .catch(error => {
          reject(error);

          // eslint-disable-next-line no-console
          console.warn(
            "There was an error connecting to the session:",
            error.code,
            error.message,
          );
        });
    });
  }

  leaveSession() {
    // Leave the session by calling 'disconnect' method over the Session object ---

    this.session.disconnect();
    this.session = null;
    this.sessionName = null;
  }

  async connect(user, sessionName) {
    // Video-call chosen by the user
    let body;
    let response;
    try {
      try {
        response = await APIService.get(
          `/api/rooms/${sessionName}`,
          {},
          "Request of finding room gone wrong",
        );
      } catch (err) {
        response = false;
      }

      if (response !== false) {
        body = { login: user.login };
        response = await APIService.post(
          `api/rooms/${sessionName}/connect`,
          { body },
          "Request of CONNECTION gone WRONG",
        );
      } else {
        body = { user: { login: user.login }, room: { roomName: sessionName } };
        response = await APIService.post(
          "api/rooms/",
          { body },
          "Request of room gone WRONG:",
        );
        body = { login: user.login };
        response = await APIService.post(
          `api/rooms/${response.name}/connect`,
          { body },
          "Request of CONNECTION gone WRONG",
        );
      }

      // eslint-disable-next-line prefer-destructuring
      this.token = response.token; // Get token from response
      // eslint-disable-next-line no-console
      console.warn(`Request of TOKEN gone WELL (TOKEN:${this.token})`);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn("Error. Can't getToken", e);
    }
  }

  initPublisher(user, videoSource, callback) {
    // --- 6) Get your own camera stream ---

    this.publisher = this.OV.initPublisher(
      `smallVideo-${user.login}`,
      {
        audioSource: undefined, // The source of audio. If undefined default microphone
        videoSource, // The source of video. If undefined default webcam
        publishAudio: true, // Whether you want to start publishing with your audio unmuted or not
        publishVideo: true, // Whether you want to start publishing with your video enabled or not
        resolution: "1280x720", // The resolution of your video
        frameRate: 30, // The frame rate of your video
        insertMode: "APPEND", // How the video is inserted in the target element 'video-container'
        mirror: false, // Whether to mirror your local video or not
      },
      callback,
    );

    // When our HTML video has been added to DOM...
    // this.publisher.on("videoElementCreated", event => {
    this.publisher.on("videoElementCreated", () => {
      store.dispatch(
        updateParticipant(user.login, {
          mediaStream: this.publisher.stream.mediaStream,
        }),
      );
      store.dispatch(setMainParticipant(user.login));
    });
    // --- 8) Publish your stream ---

    this.session.publish(this.publisher);
  }

  unpublish() {
    this.session.unpublish(this.publisher);
    this.publisher = null;
  }
}

export default new OpenViduSessionService();
