const OpenViduRole = require("openvidu-node-client").OpenViduRole;

module.exports = (sequelize, type) =>
  sequelize.define("user", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: type.STRING,
    login: type.STRING,
    password: type.STRING,
    role: {
      type: type.STRING,
      defaultValue: OpenViduRole.PUBLISHER,
    },
    token: {
      type: type.STRING,
      defaultValue: null,
    },
    refreshToken: {
      type: type.STRING,
      defaultValue: null,
    },
  });
