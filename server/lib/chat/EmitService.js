const io = require("socket.io");
const GetUsersFromRoom = require("../services/roomServices/GetUsersFromRoomService");
const UpdateUser = require("../services/userServices/UpdateUserService");
const CreateMessage = require("../services/chatServices/CreateMessageService");
const RoomDisconnect = require("../services/roomServices/RoomDisconnectService");
const GetMessages = require("../services/chatServices/GetMessagesService");
const getMessages = new GetMessages();
const roomDisconnect = new RoomDisconnect();
const getUsersFromRoom = new GetUsersFromRoom();
const updateUser = new UpdateUser();
const createMessage = new CreateMessage();

class Emit {
  constructor() {
    this.namespaces = new Map();
    this.socketsToUsers = new Map();
  }
  mount(server) {
    this.io = io(server);
    console.log(`Sockets listening on port ${process.env.PORT || 5000}`);
  }
  createNsp(roomName) {
    const nsp = this.io.of(`/${roomName}`);
    this.namespaces.set(roomName, nsp);
    nsp.on("connect", socket => {
      console.log("Connected to room ", roomName, socket.id);

      socket.on("userJoin", async user => {
        nsp.emit("userJoin", JSON.stringify(user));
        this.socketsToUsers.set(socket.id, user);
        console.log("user join", user, socket.id);
        const members = await getUsersFromRoom.execute(roomName);
        const messages = await getMessages.execute(roomName);
        socket.emit("memberList", JSON.stringify(members));
        socket.emit("messagesList", JSON.stringify(messages));
      });

      socket.on("messageSent", async body => {
        const user = this.socketsToUsers.get(socket.id);
        nsp.emit("messageSent", JSON.stringify({ ...body, user: user.login }));
        await createMessage.execute(roomName, user.id, body.message);
      });

      socket.on("muteAudio", flag => {
        const user = this.socketsToUsers.get(socket.id);
        nsp.emit("muteAudio", JSON.stringify(user), flag);
      });

      socket.on("muteVideo", flag => {
        const user = this.socketsToUsers.get(socket.id);
        nsp.emit("muteVideo", JSON.stringify(user), flag);
      });

      socket.on("raiseHand", flag => {
        const user = this.socketsToUsers.get(socket.id);
        nsp.emit("raiseHand", JSON.stringify(user), flag);
      });

      socket.on("disconnect", async () => {
        console.log("Disconnected from room ", roomName, socket.id);
        const user = this.socketsToUsers.get(socket.id);
        const room = await roomDisconnect.execute(roomName, user);
        console.log(room.empty);
        if (room.empty) {
          console.log(`Deleted empty room ${roomName}`);
          this.deleteNsp(roomName);
        }
        nsp.emit("userLeft", JSON.stringify(user));
        await updateUser.execute(user.id, { token: null, roomId: null });
        this.socketsToUsers.delete(socket.id);
      });
    });
  }
  deleteNsp(roomName) {
    this.namespaces.delete(roomName);
  }
  emitEvent(roomName, event, data) {
    const nsp = this.namespaces.get(roomName);
    console.log("connected sockets:", nsp.connected);
    nsp.emit(event, data);
  }
}
module.exports = new Emit();
