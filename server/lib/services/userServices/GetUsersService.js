const database = require("../../database");

class GetUsers {
  async execute() {
    let result = false;
    try {
      result = await database.User.findAll({
        attributes: {
          exclude: ["password", "refreshToken", "token"],
        },
      });
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}
module.exports = GetUsers;
