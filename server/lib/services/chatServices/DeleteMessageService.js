const database = require("../../database");

class DeleteMessage {
  async execute(roomId, userId, message) {
    let result = false;
    const msg = await database.Message.findOne({
      where: {
        roomId,
        userId,
        value: message,
      },
    });
    console.log(msg);
    if (msg) {
      try {
        msg.destroy();
        console.log("deleted");
        result = true;
      } catch (err) {
        console.log(err);
      }
    }
    return result;
  }
}

module.exports = DeleteMessage;
